const sections = document.querySelectorAll("section");
const bubble = document.querySelector(".bubble");
const gradients = [
  "linear-gradient(-225deg, #23074d, #cc5333)",
  "linear-gradient(-225deg, #00b09b, #96c93d)",
  "linear-gradient(-225deg, #fc4a1a, #f7b733)",
  "linear-gradient(-225deg, #667db6, #0082c8, #0082c8, #667db6)"
];

const options = {
  threshold: 0.7
};

let observer = new IntersectionObserver(navCheck, options);
function navCheck(entries) {
  entries.forEach(entry => {
    const className = entry.target.className;
    const activeAnchor = document.querySelector(`[data-page=${className}]`);
    const gradientIndex = entry.target.getAttribute("data-index");
    const coords = activeAnchor.getBoundingClientRect();
    const directions = {
      height: coords.height,
      width: coords.width,
      top: coords.top,
      left: coords.left
    };
    if (entry.isIntersecting) {
      bubble.style.setProperty("left", `${directions.left}px`);
      bubble.style.setProperty("top", `${directions.top}px`);
      bubble.style.setProperty("height", `${directions.height}px`);
      bubble.style.setProperty("width", `${directions.width}px`);
      bubble.style.setProperty("background", `${gradients[gradientIndex]}`);
      let links = Array.from(
        document.querySelector("nav").querySelectorAll("a")
      );
      links.forEach(link => {
        link.style.setProperty("color", "black");
        links[gradientIndex].style.setProperty("color", "#fff");
      });
    }
  });
}
sections.forEach(section => {
  observer.observe(section);
});
