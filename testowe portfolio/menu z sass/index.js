//copying address from header and logo move to top

let flag = false;
let copy = () => {
  if (flag === false) {
    navigator.clipboard.writeText("michal.ciombor@gmail.com");

    alert(`e-mail address is now copied`);
    flag = true;
  } else {
    alert("e-mail address is already copied!!");
  }
};

document.addEventListener("DOMContentLoaded", () => {
  let changeBurgerColor = () => {
    let burgerElement = document.querySelectorAll(".burger__element");
    let burgerElementBlack = Array.from(burgerElement);
    if (
      currentSectionIndex === 1 ||
      currentSectionIndex === 3 ||
      currentSectionIndex == 5
    ) {
      burgerElementBlack.map(
        el => (el.style.backgroundColor = "rgb(253, 101, 0)")
      );
    } else {
      burgerElementBlack.map(el => (el.style.backgroundColor = "white"));
    }
  };
  //logo change depends on width
  const logo = document.querySelector(".header__logo--img");
  if (window.innerWidth < 525) {
    logo.removeAttribute("src");
    logo.setAttribute("src", "./img/LOGO-SMALL.gif");
  } else if (window.innerWidth > 525 && window.innerWidth < 800) {
    logo.removeAttribute("src");
    logo.setAttribute("src", "./img/LOGO-MEDIUM.gif");
  }

  // mobile navigation burger
  const burger = document.querySelector(".burger");
  const mobileNav = document.querySelector(".mobile");
  const burgerElement = document.querySelectorAll(".burger__element");
  let burgerActive = Array.from(burgerElement);
  let addActive = () => {
    burgerActive.map(el => el.classList.toggle("active"));
    mobileNav.classList.toggle("active");
  };
  burger.addEventListener("click", addActive);

  //buttonDown
  const btndown = document.querySelector(".buttonDown");
  btndown.addEventListener("click", function() {
    about.scrollIntoView({ behavior: "smooth", block: "start" });
    currentSectionIndex = 1;
  });
  const header = document.querySelector(".header");
  const btn = document.createElement("button");
  const textLeft = document.querySelector(".header__text--left");
  const textRight = document.querySelector(".header__text--right");

  btn.setAttribute("class", "header__btn");
  btn.innerText = "contact";

  header.appendChild(btn);

  let check = () => {
    if (burger) {
      mobileNav.classList.remove("active");
      burgerActive.map(el => el.classList.remove("active"));
      console.log(currentSectionIndex);
      changeBurgerColor();
    }
  };
  document.querySelector(".header__btn").addEventListener("click", function() {
    const mainLogo = document.querySelector(".header__logo");
    mainLogo.style.top = "-10px";
    textLeft.innerHTML = "";
    let emailAddress = "michal.ciombor@gmail.com";
    textRight.setAttribute("style", "display:none");
    let name = () => {
      let email = "";

      let name2 = [...emailAddress];

      for (let i = 0; i < name2.length; i++) {
        setTimeout(function showName() {
          email += name2[i];
          document
            .querySelector(".header")
            .setAttribute("style", "justify-content: center");
          textLeft.innerHTML = email;
          btn.setAttribute("style", "display:none");
        }, i * 100);
      }
      return email;
    };

    textLeft.innerHTML = name();

    let btn2 = document.createElement("button");
    btn2.setAttribute("class", "header__btn--copy");
    btn2.setAttribute("onclick", "copy()");
    btn2.innerText = "COPY";

    document.querySelector(".header__text").appendChild(btn2);
  });
  //nav handle click
  const about = document.querySelector(".mainSection__about");
  const skills = document.querySelector(".mainSection__skills");
  const projects = document.querySelector(".mainSection__projects");
  const contact = document.querySelector(".mainSection__contact");
  const others = document.querySelector(".mainSection__others");
  const navElement = document.querySelectorAll(".element--nav");

  navElement.forEach(el =>
    el.addEventListener("click", function(e) {
      switch (this.id) {
        case "about": {
          about.scrollIntoView({ behavior: "smooth", block: "start" });
          currentSectionIndex = 1;
          check(currentSectionIndex);
          break;
        }
        case "skills": {
          skills.scrollIntoView({ behavior: "smooth", block: "start" });
          currentSectionIndex = 2;
          check(currentSectionIndex);
          break;
        }
        case "projects": {
          currentSectionIndex = 3;

          projects.scrollIntoView({ behavior: "smooth", block: "start" });
          check(currentSectionIndex);
          break;
        }
        case "contact": {
          contact.scrollIntoView({ behavior: "smooth", block: "start" });
          currentSectionIndex = 4;
          check(currentSectionIndex);
          break;
        }
        case "others": {
          others.scrollIntoView({ behavior: "smooth", block: "start" });
          currentSectionIndex = 5;
          check(currentSectionIndex);
          break;
        }
      }
    })
  );
  //btn backToTop
  const BackToTop = document.querySelector(".backToTop");
  document.addEventListener("scroll", () => {
    if (window.scrollY > 160) {
      BackToTop.classList.add("active");
      document.querySelectorAll("a").forEach(el => el.classList.add("active"));
      document.querySelector(".navigation").classList.add("active");
    } else {
      BackToTop.classList.remove("active");
      document
        .querySelectorAll("a")
        .forEach(el => el.classList.remove("active"));
      document.querySelector(".navigation").classList.remove("active");
    }
    BackToTop.addEventListener("click", () => {
      window.scrollTo({ top: 0, behavior: "smooth" });
    });
  });

  // obsluga scrollera
  let currentSectionIndex = 0;
  let isThrottled = false;

  document.addEventListener("mousewheel", e => {
    const sections = document.querySelectorAll(".mainSection");

    const direction = e.wheelDelta > 0 ? -1 : 1;
    if (isThrottled) return;
    isThrottled = true;
    setTimeout(() => {
      isThrottled = false;
    }, 1000);

    if (direction === 1) {
      const isLastSection = currentSectionIndex === sections.length - 1;
      if (isLastSection) return;
    }
    if (direction === -1) {
      const isFirstSection = currentSectionIndex === 0;
      if (isFirstSection) return;
    }

    currentSectionIndex = currentSectionIndex + direction;
    // currentSectionIndex === 3 ? check(3) : check(1);
    console.log(currentSectionIndex);
    sections[currentSectionIndex].scrollIntoView({
      behavior: "smooth",
      block: "start"
    });
    changeBurgerColor();
  });
});
